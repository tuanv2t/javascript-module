//module.js
export default class {
  constructor() {
    console.log("Default export");
  }
}

export class myModule {
  constructor() {
    console.log("myModule");
  }
  
}

export const url = "https://sites.google.com/view/angular-new/00-overview/introduce-modules-in-ecmascript-2015";

export function myMethod() {
  console.log("myMethod");
}